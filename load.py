# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import django
from pprint import pprint


def create_records():
    from app import models

    author = models.Author.objects.create(name='author2')
    models.Book.objects.create(author=author, title='title3')
    models.Book.objects.create(author=author, title='title4')

main = create_records


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'main.settings')
    django.setup()
    main()
