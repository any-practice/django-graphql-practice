from rest_framework import serializers

from app import models


class AuthorSerializer(serializers.ModelSerializer):

    books = serializers.SlugRelatedField(
        many=True,
        slug_field='title',
        read_only=True,
    )

    class Meta:
        model = models.Author
        fields = ['name', 'books']
