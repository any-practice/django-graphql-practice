from django.urls import path

from graphene_django.views import GraphQLView

from . import views

urlpatterns = [
    path('author_detail/<int:author_id>/', views.AuthorDetailView.as_view()),
    path("graphql/", GraphQLView.as_view(graphiql=True)),
]
