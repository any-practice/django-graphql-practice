from django.db.models import Prefetch

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from app import models, serializers


class AuthorDetailView(APIView):
    def _get_author(self, author_id) -> models.Author:
        author = models.Author.objects \
            .prefetch_related(
                Prefetch(
                    'books',
                    queryset=models.Book.objects.only('title'),
                ),
            ) \
            .get(id=author_id)
        return author

    def get(self, request, author_id):
        author = self._get_author(author_id=author_id)
        return Response(serializers.AuthorSerializer(author).data, status=status.HTTP_200_OK)
