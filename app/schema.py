import graphene
from graphene_django import DjangoObjectType, DjangoListField

from . import models


class BookType(DjangoObjectType):
    class Meta:
        model = models.Book
        fields = ('id', 'title')


class AuthorType(DjangoObjectType):

    books = DjangoListField(BookType)

    # def resolve_books(root, info):
    #     return models.Book.objects.all()

    class Meta:
        model = models.Author
        fields = ('id', 'name', 'books')


class Query(graphene.ObjectType):

    all_authors = graphene.List(AuthorType)
    book_by_title = graphene.Field(BookType, title=graphene.String(required=True))

    def resolve_all_authors(root, info):
        return models.Author.objects.prefetch_related('books').all()

    def resolve_book_by_title(root, info, title):
        try:
            return models.Book.objects.filter(title=title).first()
        except Exception:
            return None


schema = graphene.Schema(query=Query)